FROM java:8
COPY ./target/tm-frontend.war /opt/tm-frontend.war
WORKDIR /opt

EXPOSE 9090
ENTRYPOINT ["java", "-jar", "tm-frontend.war"]